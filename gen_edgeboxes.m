function [ ] = gen_edgeboxes(scenario)

    image_db = '/home/isit/workspace/DefoObjInWild/dataset/';
    % set opts for training (see edgesTrain.m)
    opts=edgesTrain();                % default options (good settings)
    opts.modelDir='models/';          % model will be in models/forest
    opts.modelFnm='modelBsds';        % model name
    opts.nPos=5e5; opts.nNeg=5e5;     % decrease to speedup training
    opts.useParfor=0;                 % parallelize if sufficient memory
    % train edge detector (~20m/8Gb per tree, proportional to nPos/nNeg)
    tic,model = edgesTrain(opts); toc; % will load model if already trained

    % for training set
    if (strcmp(scenario,'train') == 1)
        % image_db = '/home/isit/workspace/object_dataset';
        image_filenames = textread([image_db '/ImageSets/train.txt'], '%s', 'delimiter', '\n');
        for i = 1:length(image_filenames)
            if exist([image_db '/Images/' image_filenames{i} '.jpg'], 'file') == 2
                image_filenames{i} = [image_db '/Images/' image_filenames{i} '.jpg'];
            else
                error('File does not exist. Please check!!');
            end
            if exist([image_db '/Images/' image_filenames{i} '.png'], 'file') == 2
                image_filenames{i} = [image_db '/Images/' image_filenames{i} '.png'];
            end
            I = imread(image_filenames{i});
            all_boxes{i} = format_boxes(edgeBoxes(I, model, 'alpha',0.4));
        end
        fprintf('Done with generating TRAIN edge boxes of size: %d %d \n', size(all_boxes));
        save('train_proposals.mat', 'all_boxes', '-v7');
    end
    
    % for test set
    if (strcmp(scenario,'test') == 1)
        %image_db = '/home/isit/workspace/object_dataset';
        %image_filenames = textread([image_db '/data/ImageSets/test.txt'], '%s', 'delimiter', '\n');
        image_filenames = textread([image_db '/ImageSets/test.txt'], '%s', 'delimiter', '\n');
        for i = 1:length(image_filenames)
            if exist([image_db '/TestImages/' image_filenames{i} '.jpg'], 'file') == 2
                image_filenames{i} = [image_db '/TestImages/' image_filenames{i} '.jpg'];
            else
                error('File does not exist. Please check!!');
            end
            if exist([image_db '/TestImages/' image_filenames{i} '.png'], 'file') == 2
                image_filenames{i} = [image_db '/TestImages/' image_filenames{i} '.png'];
            end
            I = imread(image_filenames{i});
            all_boxes{i} = format_boxes(edgeBoxes(I, model, 'alpha',0.4));
        end
        fprintf('Done with generating TEST edge boxes of size: %d %d \n', size(all_boxes));
        save('test_proposals.mat', 'all_boxes', '-v7');
    end
    
    
    function op_box = format_boxes(boxes)
        % ip_box is of format [x y w h]
        % op_box is of format [xmin ymin xmax ymax]
        parfor j = 1:size(boxes,1)
            xmin = boxes(j,1);
            ymin = boxes(j,2);
            xmax = boxes(j,1) + boxes(j,3);
            ymax = boxes(j,2) + boxes(j,4);
            op_box(j,:) = [xmin ymin xmax ymax];
        end
    end
    
end