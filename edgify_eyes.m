clc; clear ; close all;

syntheyes_src_imgs = dir('/media/shrinivasan/DATA/datasets/syntheyes_images_gray_small/images/*.png');
syntheyes_dest_dir = '/media/shrinivasan/DATA/datasets/syntheyes_images_gray_small_edges/images/';
model = initEdges()

for i = 1 : length(syntheyes_src_imgs)
    img = imread(strcat( ...
        '/media/shrinivasan/DATA/datasets/syntheyes_images_gray_small/images/',syntheyes_src_imgs(i).name));
    curr_img(:,:,1) = img; curr_img(:,:,2) = img; curr_img(:,:,3) = img;
    edges = edgesDetect(curr_img,model);
    % normalize 
    edges = mat2gray(edges);
    imwrite(edges, strcat(syntheyes_dest_dir, syntheyes_src_imgs(i).name));
    sprintf('Done with image %d \n', i)
end

%%
mpiigaze_src_imgs = dir('/media/shrinivasan/DATA/datasets/mpiigaze_images/images/*.jpg');
mpiigaze_dest_dir = '/media/shrinivasan/DATA/datasets/mpiigaze_images_edges/images/';
for j = 1 : length(mpiigaze_src_imgs)
    img = imread(strcat( ...
        '/media/shrinivasan/DATA/datasets/mpiigaze_images/images/',mpiigaze_src_imgs(j).name));
    curr_img(:,:,1) = img; curr_img(:,:,2) = img; curr_img(:,:,3) = img;
    edges = edgesDetect(curr_img,model);
    % normalize 
    edges = mat2gray(edges);
    imwrite(edges, strcat(mpiigaze_dest_dir, mpiigaze_src_imgs(j).name));
    sprintf('Done with image %d \n', j)
end